## 如何使用：<br/>
1. 給要儲存*list*的實體加上`merge_sort`的tag<br/>
2. 確認該實體`HandItems[0]`欄位有持有道具<br/>
3. 執行`/execute as @e[tag=merge_sort] run function merge_sort:setup`<br/>
4. 將要排序的*list*儲存在該實體的`HandItems[0].tag.array`中<br/>
5. 執行`/function merge_sort:run`<br/>
6. 結果會儲存在`HandItems[0].tag.array`中<br/>