execute as @e[tag=merge_sort] unless data entity @s HandItems[0].tag.array run tellraw @a ["Error: No argument in HandItems[0].tag.array"]

execute as @e[tag=merge_sort] if data entity @s HandItems[0].tag.array run data modify entity @s HandItems[0].tag.argument prepend from entity @s HandItems[0].tag.array
execute as @e[tag=merge_sort] if data entity @s HandItems[0].tag.array run function merge_sort:recursive