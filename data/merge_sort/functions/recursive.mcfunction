# Get array length
execute store result score @s merge_sort_num run data get entity @s HandItems[0].tag.argument[0]

# Base case
execute if score @s merge_sort_num matches 1 run function merge_sort:base_case

# Split and merge sort them
execute if score @s merge_sort_num matches 2.. run function merge_sort:split_and_sort