execute store result score @s merge_sort_left run data get entity @s HandItems[0].tag.return[1][0]
execute store result score @s merge_sort_right run data get entity @s HandItems[0].tag.return[0][0]

# set '<=' to keep original order of same values 
execute if score @s merge_sort_left <= @s merge_sort_right run data modify entity @s HandItems[0].tag.array append from entity @s HandItems[0].tag.return[1][0]
execute if score @s merge_sort_left <= @s merge_sort_right run data remove entity @s HandItems[0].tag.return[1][0]
execute if score @s merge_sort_left > @s merge_sort_right run data modify entity @s HandItems[0].tag.array append from entity @s HandItems[0].tag.return[0][0]
execute if score @s merge_sort_left > @s merge_sort_right run data remove entity @s HandItems[0].tag.return[0][0]

# if no elements on the right half
execute unless data entity @s HandItems[0].tag.return[0][] run function merge_sort:flush_left
# else if no elements on the left half
execute unless data entity @s HandItems[0].tag.return[1][] if data entity @s HandItems[0].tag.return[0][] run function merge_sort:flush_right

execute if data entity @s HandItems[0].tag.return[0][] run function merge_sort:compare