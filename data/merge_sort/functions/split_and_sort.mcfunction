scoreboard players operation @s merge_sort_num /= #CONST_2 merge_sort_num

# Split the left half and sort
function merge_sort:push_left

# sort the right half
function merge_sort:recursive

# Merge left and right
function merge_sort:merge