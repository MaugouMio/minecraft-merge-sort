data modify entity @s HandItems[0].tag.argument[0] append from entity @s HandItems[0].tag.argument[1][0]
data remove entity @s HandItems[0].tag.argument[1][0]
scoreboard players remove @s merge_sort_num 1

execute if score @s merge_sort_num matches 1.. run function merge_sort:push_left_recursive